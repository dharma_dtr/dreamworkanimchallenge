//Globals Related to Sparks
//var speedFactor = 0.05; // Controls speed with which velocity is applied
//var gravity = -9.8; // Gravity
//var sparkSize = 15 * 0.01;
var SparkSystem = function () {
	this.sparksArray = [];
	this.velocity = {
		x : 7,
		y : 2,
		z : 0
	};
	this.velocityRandomness = {
		x : 0,
		y : 0,
		z : 0
	};
	this.position = {
		x : -0.5,
		y : 1.2,
		z : 0
	};
	this.positionRandomness = {
		x : 0,
		y : 0,
		z : 0
	};
	this.sparkCreationRate = 0;
	this.sparkCreationRateRandomness = 0;
	this.sparkSize = 0.2;
	this.sparkSizeRandomness = 0;
	this.sparkLifespan = 100;
	this.sparkLifespanRandomness = 0;
	this.collisionObjects = [];
	this.collisionLights = [];
	this.splitOnCollision = false;
};

SparkSystem.prototype = {
	createNewSpark : function (pos, vel, lifespan, size) {
		//NextID
		var newId = 0;
		var len = this.sparksArray.length;
		if (len != 0) {
			newId = this.sparksArray[len - 1].id + 1;
		}
		//Position
		var position = pos || Utils.getRandomValueXYZ(this.position, this.positionRandomness);
		//Velocity
		var velocity = vel || Utils.getRandomValueXYZ(this.velocity, this.velocityRandomness);
		velocity.z = getRandomArbitrary(velocity.z*-1,velocity.z); //QuickFix for RandomVelocity in Z
		//Lifespan
		var lifeSpan = lifespan || Utils.getRandomValue(this.sparkLifespan, this.sparkLifespanRandomness);
		//Size
		var sparkSize = size || Utils.getRandomValue(this.sparkSize, this.sparkSizeRandomness);

		//Create a new SparkObject
		var newSparkObject = new Spark(newId, position, velocity, lifeSpan, sparkSize);
		//Add it to SparkSystem
		this.sparksArray.push(newSparkObject);

	},
	updateSparks : function () {
		var len = this.sparksArray.length;
		while (len--) {
			var sparkObj = this.sparksArray[len];
			var sparkMesh = scene.getObjectByName(sparkObj.id);
			
			// Check if Spark is dead
			if (sparkObj.lifeSpan <= 0) {
				// remove spark & object
				this.killSparkAt(len);
				if (len != 0)
					len++;
				continue;
			}
			
			sparkObj.update();
			//Check if Spark is Collided 
			if(sparkObj.detectCollision(this.collisionObjects)) {
				
				//Create New Sparks - if meets some criterias
				var vel = new THREE.Vector3(sparkObj.velocity.x,sparkObj.velocity.y,sparkObj.velocity.z);
				var sparkMesh = scene.getObjectByName(sparkObj.id);
				var minScale = 0.09;
				//if(Math.abs(vel.y) > minVelocity || Math.abs(vel.x) > minVelocity || Math.abs(vel.z) > minVelocity) {
				if(sparkMesh.scale.x > minScale ) {	
					//DotProduct Symmetry u · v = v · u
					//Reflection r = a - 2<a, n> n
					
					var newVelocity = vel.reflect(sparkObj.collisionNormal);
					
					var scale = sparkMesh.scale.x * 0.5;
					sparkObj.velocity = newVelocity;
					sparkMesh.scale.set(scale,scale,scale);
					//this.createNewSpark(sparkMesh.position,newVelocity,sparkObj.lifeSpan,sparkMesh.scale.x * 0.50);
					if(this.splitOnCollision) {
						var num = getRandomInt(1,3);
						var moreVel = Utils.splitVelocities(vel,num);
						for(var i=0; i<num; i++) { 
							var tmp = newVelocity.clone();
							tmp.applyAxisAngle(sparkObj.collisionNormal, getRandomInt(1,360));
							this.createNewSpark(sparkObj.collisionPoint,tmp,getRandomInt(0,sparkObj.lifeSpan),sparkMesh.scale.x);
						}
					}
					sparkObj.collisionPoint = null;
				} else {
					//mark it to remove
					sparkObj.lifeSpan = 0;
				}
				
			}
			
		}
	},
	killSparkAt : function(index) {
		var sparkObj = this.sparksArray[index];
		var sparkMesh = scene.getObjectByName(sparkObj.id);
		scene.remove(sparkMesh);
		this.sparksArray.splice(index, 1);
	},
	addCollisionObject : function(obj) {
		this.collisionObjects.push(obj);
	}
};
Utils = {

	getRandomValueXYZ : function (value, randomness) {
		var randomValue = {
			x : this.getRandomValue(value.x, randomness.x),
			y : this.getRandomValue(value.y, randomness.y),
			z : this.getRandomValue(value.z, randomness.z),
		};

		return randomValue;
	},

	getRandomValue : function (value, randomness) {
		var variableValue = value * randomness * 0.01;
		var randomValue = getRandomArbitrary(value - variableValue, value + variableValue);
		return randomValue;
	},
	
	//ToDo : Implement Original Functionality
	splitVelocities : function(velocity,num) {
		var res = [{
			x : -velocity.x ,
			y : velocity.y ,
			z : velocity.z 
		},{
			x : velocity.x ,
			y : -velocity.y ,
			z : velocity.z 
		},{
			x : velocity.x ,
			y : velocity.y ,
			z : -velocity.z 
		}];
			
		return res;
	}

};

//Globals
var Globals = {
	physicsFactor : 0.02,
	gravity : -9.8,
	airDrag : 1.2,
	animate : true,
	conePosition : {
		x : -5,
		y : 0,
		z : 0
	},
	cubePosition : {
		x : -3,
		y : 0,
		z : 2
	},
	spherePosition : {
		x : -3,
		y : 0,
		z : -2
	},
	bunnyPosition : {
		x : -2,
		y : 0,
		z : 0
	},
	createSparkOnMouseClick : false
};
var sprMap = new THREE.TextureLoader().load( "assets/sprite.png" );
var sprMaterial = new THREE.SpriteMaterial( { map: sprMap, color: 0xffffff, fog: true } );

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
function getRandomArbitrary(min, max) {
	return Math.random() * (max - min) + min;
}

/**
 * Sparks. Represents individual spark.
 */
// Constructor
var Spark = function (id, position, velocity, life,sparkSize) {
	// id of spark
	this.id = id;
	// velocity of spark
	this.velocity = {
		x : velocity.x || 0,
		y : velocity.y || 0,
		z : velocity.z || 0
	};
	// Create 3d mesh for Spark
	var sparkMesh = new THREE.Sprite(sprMaterial);
	sparkMesh.scale.set(sparkSize, sparkSize, sparkSize);
	sparkMesh.position.set(position.x,position.y,position.z);
	sparkMesh.name = id;
	scene.add(sparkMesh);
	
	this.lifeSpan = life;
	//Direction vector
	this.direction = new THREE.Vector3();
	// If these are null, means - no collision is detected 
	this.collisionPoint = null;
	this.collisionNormal = null;
	
};
// Methods for Spark Object
Spark.prototype = {
	// Updates Spark Motion & behaviour
	update : function () {

		// Gravity applies on Velocity_Y
		this.velocity.y += Globals.gravity * Globals.physicsFactor;
		//Apply AirResistance
		this.velocity.x -= Globals.airDrag * Globals.physicsFactor * this.velocity.x;
		this.velocity.y -= Globals.airDrag * Globals.physicsFactor * this.velocity.y;
		this.velocity.z -= Globals.airDrag * Globals.physicsFactor * this.velocity.z;
		// Translate 3d mesh
		var sparkMesh = scene.getObjectByName(this.id);
		var oldPosition = sparkMesh.position.clone();
		//If Collision is detected in previous frame, do not penetrate through surface of collision object (see else case)
		if(this.collisionPoint === null) { 
			translatePosition(sparkMesh, this.velocity.x * Globals.physicsFactor, this.velocity.y
			 * Globals.physicsFactor, this.velocity.z * Globals.physicsFactor);
		} else {
			var offset = this.direction.clone().setLength(this.collisionLength);
			sparkMesh.position.set(this.collisionPoint.x,this.collisionPoint.y ,this.collisionPoint.z);
		}
		
		// Update LifeSpan
		this.lifeSpan--;
		//Update direction
		var newPosition = sparkMesh.position.clone();
		this.direction = newPosition.sub(oldPosition);
	},
	
	detectCollision : function (collisionObjects) {
		var sparkMesh = scene.getObjectByName(this.id);
		var currentPosition = sparkMesh.position.clone();
		var ray = new THREE.Raycaster(currentPosition, this.direction.clone().normalize());
		var collisionResults = ray.intersectObjects(collisionObjects,true);
		if(collisionResults.length > 0) {
			//Ray passing with atleast one object
			var distance = collisionResults[0].distance;
			var lengthOfDirection = this.direction.length();
			if(this.collisionPoint === null && distance - lengthOfDirection < 0) {
				//Means, Spark will collide with collision object in next frame.
				this.collisionPoint = collisionResults[0].point;
				this.collisionNormal = collisionResults[0].face.normal;
				//Return false as Spark is not yet collided
				return false;
			}
			
		}
		//return (this.collisionLength === -999) ? (false) : (true);
		return (this.collisionPoint === null) ? (false) : (true);
	}
};


