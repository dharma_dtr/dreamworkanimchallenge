//Globals Related to Sparks
//var speedFactor = 0.05; // Controls speed with which velocity is applied
//var gravity = -9.8; // Gravity
//var sparkSize = 15 * 0.01;
var SparkSystem = {
	sparksArray : [],
	velocity : {
		x : 10,
		y : 5,
		z : 1
	},
	velocityRandomness : {
		x : 10,
		y : 10,
		z : 5
	},
	getRandomVelocity : function () {
		var randomVelocity = this.getRandomValueXYZ(this.velocity, this.velocityRandomness);
		//We want -z to be part of random.
		randomVelocity.z = getRandomArbitrary(randomVelocity.z * -1, randomVelocity.z);
		return randomVelocity;
	},

	getRandomValueXYZ : function (value, randomness) {
		var randomValue = {
			x : this.getRandomValue(value.x, randomness.x),
			y : this.getRandomValue(value.y, randomness.y),
			z : this.getRandomValue(value.z, randomness.z),
		};

		return randomValue;
	},

	position : {
		x : -0.5,
		y : 1.2,
		z : 0
	},
	positionRandomness : {
		x : 5,
		y : 5,
		z : 0
	},
	getRandomPosition : function () {
		var res = {
			x : this.getRandomValue(this.position.x, this.positionRandomness.x),
			y : this.getRandomValue(this.position.y, this.positionRandomness.y),
			z : this.getRandomValue(this.position.z, this.positionRandomness.z)
		};

		return res;
	},

	getAirResistance : function (velocity) {
		var res = {
			x : this.velocity.x * Globals.airDrag * Globals.physicsFactor,
			y : this.velocity.y * Globals.airDrag * Globals.physicsFactor,
			z : this.velocity.z * Globals.airDrag * Globals.physicsFactor
		};
		return res;
	},

	sparkCreationRate : 8,
	randomSparkCreationRate : 25,
	getRandomSparkCreationRate : function () {
		var randomCreationRate = this.getRandomValue(this.sparkCreationRate, this.randomSparkCreationRate);
		return randomCreationRate;
	},

	getRandomValue : function (value, randomness) {
		var variableValue = value * randomness * 0.01;
		var randomValue = getRandomArbitrary(value - variableValue, value + variableValue);
		return randomValue;
	},
	sparkSize : 0.1,
	sparkSizeRandomness : 100,
	getRandomSparkSize : function () {
		var randomSize = this.getRandomValue(this.sparkSize, this.sparkSizeRandomness);
		return randomSize;
	},
	sparkLifespan : 15,
	sparkLifespanRandomness : 50,
	getRandomSparkLifespan : function () {
		var randomSpan = this.getRandomValue(this.sparkLifespan, this.sparkLifespanRandomness);
		return randomSpan;
	}
};

//Globals
var Globals = {
	physicsFactor : 0.01,
	gravity : -9.8,
	airDrag : 1.2,
	animate : true
};

var datGui = new dat.GUI();
var sprMap = new THREE.TextureLoader().load( "assets/sprite.png" );
var sprMaterial = new THREE.SpriteMaterial( { map: sprMap, color: 0xffffff, fog: true } );
                

var SparkUtil = {
	createSpark : function (pos,vel) {
		// Calculate NextID of Spark
		var newId = 0;
		var len = SparkSystem.sparksArray.length;
		if (len != 0) {
			newId = SparkSystem.sparksArray[len - 1].id + 1;
		}
		
		// Create New Spark
		var newSpark = new Spark(newId,pos || SparkSystem.getRandomPosition(),vel || SparkSystem.getRandomVelocity());
		SparkSystem.sparksArray.push(newSpark);
	},
	updateSparks : function () {

		var len = SparkSystem.sparksArray.length;
		while (len--) {
			var sparkObj = SparkSystem.sparksArray[len];
			// 1. Check for life expairy
			if (sparkObj.lifeSpan <= 0) {
				// remove spark & object
				//console.log(sparkObj.id + " : Life is ended");
				var sparkMesh = scene.getObjectByName(sparkObj.id);
				scene.remove(sparkMesh);
				SparkSystem.sparksArray.splice(len, 1);
				if (len != 0)
					len++;
				continue;
			}
			// 2. Update Spark
			sparkObj.update();
		}
	}
};
function createSpark() {
	SparkUtil.createSpark();
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
function getRandomArbitrary(min, max) {
	return Math.random() * (max - min) + min;
}
/*
 * 1. Check if spark life is ended. 2. If not, Update position
 */
function updateSparks() {

	SparkUtil.updateSparks();
}

/**
 * Sparks. Represents individual spark.
 */
// Constructor
var Spark = function (id, position, velocity,life) {
	// id of spark
	this.id = id;
	// velocity of spark
	this.velocity = {
		x : velocity.x || 0,
		y : velocity.y || 0,
		z : velocity.z || 0
	};
	// Create 3d mesh for Spark
	sparkSize = SparkSystem.getRandomSparkSize();
	//var sparkMesh = new THREE.Mesh(new THREE.CubeGeometry(sparkSize, sparkSize,
	//			sparkSize), new THREE.MeshNormalMaterial());
	var sparkMesh = new THREE.Sprite( sprMaterial );
    sparkMesh.scale.set(sparkSize,sparkSize,sparkSize);            
	sparkMesh.position.x = position.x || 0;
	sparkMesh.position.y = position.y || 0;
	sparkMesh.position.z = position.z || 0;
	sparkMesh.name = id;
	scene.add(sparkMesh);

	this.lifeSpan = life || SparkSystem.getRandomSparkLifespan();
	this.collided = false;
	//	this.lifeSpan = 10 ;
};
// Methods for Spark Object
Spark.prototype = {
	// Updates Spark Motion & behaviour
	update : function () {
		
		// Gravity applies on Velocity_Y
		this.velocity.y += Globals.gravity * Globals.physicsFactor;
		//Apply AirResistance
		var airRes = SparkSystem.getAirResistance(this.velocity);
		this.velocity.x -= airRes.x;
		this.velocity.y -= airRes.y;
		this.velocity.z -= airRes.z;
		// Translate 3d mesh
		var sparkMesh = scene.getObjectByName(this.id);
		translatePosition(sparkMesh, this.velocity.x * Globals.physicsFactor, this.velocity.y
			 * Globals.physicsFactor, this.velocity.z * Globals.physicsFactor);
		// Update LifeSpan
		this.lifeSpan--;
		this.detectCollision();
	},
	detectCollision : function () {
		/*
		var sparkMesh = scene.getObjectByName(this.id);
		var originPoint = sparkMesh.position.clone();
		var collisionObjects = [];
		collisionObjects.push(scene.getObjectByName("ground"));
		//for (var i = 0; i < sparkMesh.geometry.vertices.length; i++) {
			//var localVertex = sparkMesh.geometry.vertices[i].clone();
			//var globalVertex = localVertex.applyMatrix4(sparkMesh.matrix);
			var horizantalVector = new THREE.Vector3(0,-originPoint.y,0);
			var directionVector = horizantalVector.sub(sparkMesh.position);

			var ray = new THREE.Raycaster(originPoint, directionVector.clone().normalize());
			
			var collisionResults = ray.intersectObjects(collisionObjects,true);
			if (collisionResults.length > 0) {
				console.log("Res");
			}
			//if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length())
			//	console.log(" Hit ");
			if (collisionResults.length > 0 && collisionResults[0].distance === 0)
				console.log(" Hit ");
		//}
	
	*/
	var sparkMesh = scene.getObjectByName(this.id);
	var currentPosition = sparkMesh.position;
	if(currentPosition.y <= 0.1) {
		this.lifeSpan = 0;
		//Collided with ground. Create more sparks
		if(this.velocity.y * -1/2 < 0.2)
			return;
		var newVelocity = {
			x : this.velocity.x,
			y : this.velocity.y * -1/2,
			z : this.velocity.z
		};
		SparkUtil.createSpark(currentPosition,newVelocity, this.lifeSpan);
		
	}
		
	}
};

function drawControls() {
	var physicsFolder = datGui.addFolder("Physics");
	physicsFolder.add(Globals, "physicsFactor").max(1).min(0).step(0.01);
	//.onFinishChange(function () {
	//	Globals.physicsFactor *= 0.01;
	//});
	physicsFolder.add(Globals, "airDrag").name("Air Density").max(10).min(0).step(0.2);

	var velocityFolder = datGui.addFolder("Velocity");
	//datGui.add(SparkSystem.velocity, "x").name("Velocity X").max(100).min(0).step(1);
	//datGui.add(SparkSystem.velocity, "y").name("Velocity Y").max(100).min(0).step(1);
	//datGui.add(SparkSystem.velocity, "z").name("Velocity Z").max(100).min(0).step(1);
	velocityFolder.add(SparkSystem.velocity, "x").name("X").max(100).min(0).step(1);
	velocityFolder.add(SparkSystem.velocity, "y").name("Y").max(100).min(0).step(1);
	velocityFolder.add(SparkSystem.velocity, "z").name("Z").max(100).min(0).step(1);

	//datGui.add(SparkSystem.velocityRandomness, "x").name("VelocityRand X").max(100).min(0).step(1);
	//datGui.add(SparkSystem.velocityRandomness, "y").name("VelocityRand Y").max(100).min(0).step(1);
	//datGui.add(SparkSystem.velocityRandomness, "z").name("VelocityRand Z").max(100).min(0).step(1);
	var velocityRandomFolder = datGui.addFolder("Random Velocity");
	velocityRandomFolder.add(SparkSystem.velocityRandomness, "x").name("VelocityRand X").max(100).min(0).step(1);
	velocityRandomFolder.add(SparkSystem.velocityRandomness, "y").name("VelocityRand Y").max(100).min(0).step(1);
	velocityRandomFolder.add(SparkSystem.velocityRandomness, "z").name("VelocityRand Z").max(100).min(0).step(1);

	var positionFolder = datGui.addFolder("Spark Orign Position");
	positionFolder.add(SparkSystem.position, "x").name("Position X").max(10).min(-10).step(0.1);
	positionFolder.add(SparkSystem.position, "y").name("Position Y").max(10).min(-10).step(0.1);
	positionFolder.add(SparkSystem.position, "z").name("Position Z").max(10).min(-10).step(0.1);

	var positionRandomFolder = datGui.addFolder("Random Position");
	positionRandomFolder.add(SparkSystem.positionRandomness, "x").name("PositionRand X").max(100).min(0).step(1);
	positionRandomFolder.add(SparkSystem.positionRandomness, "y").name("PositionRand Y").max(100).min(0).step(1);
	positionRandomFolder.add(SparkSystem.positionRandomness, "z").name("PositionRand Z").max(100).min(0).step(1);

	var sparkFolder = datGui.addFolder("Spark");
	sparkFolder.add(SparkSystem, "sparkCreationRate").name("Sparks/Frame").max(50).min(0).step(1);
	sparkFolder.add(SparkSystem, "randomSparkCreationRate").name("Spark/Frame randomness").max(100).min(0).step(1);
	sparkFolder.add(SparkSystem, "sparkSize").name("Sparks/Frame").max(1).min(0.001).step(0.01);
	sparkFolder.add(SparkSystem, "sparkSizeRandomness").name("Spark/Frame randomness").max(100).min(0).step(1);
	sparkFolder.add(SparkSystem, "sparkLifespan").name("Sparks LifeSpan").max(100).min(1).step(1);
	sparkFolder.add(SparkSystem, "sparkLifespanRandomness").name("Spark LifeSpan randomness").max(100).min(0).step(1);

	datGui.add(Globals, "animate").name("Animate Scene");

};
