// Set up three.js global variables
var scene, camera, renderer, container, loadingManager;
// Set up avatar global variables
var bbox;
// Transfer global variables
var i_share = 0, n_share = 1, i_delta = 0.0;
//SparkSystems
var sparkSystem;
// Sets up the scene.
function init() {
	// Create the scene and set the scene size.
	scene = new THREE.Scene();

	// keep a loading manager
	loadingManager = new THREE.LoadingManager();

	// Get container information
	container = document.createElement('div');
	document.body.appendChild(container);

	var WIDTH = window.innerWidth,
	HEIGHT = window.innerHeight; //in case rendering in body


	// Create a renderer and add it to the DOM.
	renderer = new THREE.WebGLRenderer({
			antialias : true
		});
	renderer.setSize(WIDTH, HEIGHT);
	// Set the background color of the scene.
	renderer.setClearColor(0x000000, 1);
	//document.body.appendChild(renderer.domElement); //in case rendering in body
	container.appendChild(renderer.domElement);

	// Create a camera, zoom it out from the model a bit, and add it to the scene.
	camera = new THREE.PerspectiveCamera(45.0, WIDTH / HEIGHT, 0.01, 100);
	camera.position.set(-2, 2, -5);
	//camera.lookAt(new THREE.Vector3(5,0,0));
	scene.add(camera);

	// Create an event listener that resizes the renderer with the browser window.
	window.addEventListener('resize',
		function () {
		var WIDTH = window.innerWidth,
		HEIGHT = window.innerHeight;
		renderer.setSize(WIDTH, HEIGHT);
		camera.aspect = WIDTH / HEIGHT;
		camera.updateProjectionMatrix();
	});
	
	// Create a light, set its position, and add it to the scene.
	var alight = new THREE.AmbientLight(0x44678e);
	alight.position.set(-100.0, 200.0, 100.0);
	scene.add(alight);
	
	var light = new THREE.PointLight( 0xffa02d, 100, 50 );
	light.position.set( -0.5, 0.1, 0 );
	light.name = "sparkLight"
	scene.add( light );
	
	/*
	var directionalLight = new THREE.DirectionalLight( 0xffeedd );
	          directionalLight.position.set( 0, 5, 0 );
				//directionalLight.castShadow = true;
	              scene.add( directionalLight );

	*/
	

	// Load in the mesh and add it to the scene.
	var sawBlade_texPath = 'assets/sawblade.jpg';
	var sawBlade_objPath = 'assets/sawblade.obj';
	OBJMesh(sawBlade_objPath, sawBlade_texPath, "sawblade");

	var ground_texPath = 'assets/ground_tile.jpg';
	var ground_objPath = 'assets/ground.obj';
	OBJMesh(ground_objPath, ground_texPath, "ground");

	var slab_texPath = 'assets/slab.jpg';
	var slab_objPath = 'assets/slab.obj';
	OBJMesh(slab_objPath, slab_texPath, "slab");

	
	 //Stanford Bunny
	var bunny_texPath = 'assets/rocky.jpg';
	var bunny_objPath = 'assets/stanford_bunny.obj';
	OBJMesh(bunny_objPath, bunny_texPath, "bunny");
	 

	 //Sphere
	var sphere_texPath = 'assets/rocky.jpg';
	var sphere_objPath = 'assets/sphere.obj';
	OBJMesh(sphere_objPath, sphere_texPath, "sphere");
	 
	
	 //Cube
	var cube_texPath = 'assets/rocky.jpg';
	var cube_objPath = 'assets/cube.obj';
	OBJMesh(cube_objPath, cube_texPath, "cube");
	

	 //Cone
	var cone_texPath = 'assets/rocky.jpg';
	var cone_objPath = 'assets/cone.obj';
	OBJMesh(cone_objPath, cone_texPath, "cone");
	 

	// Add OrbitControls so that we can pan around with the mouse.
	controls = new THREE.OrbitControls(camera, renderer.domElement);

	controls.enableDamping = true;
	controls.dampingFactor = 0.4;
	controls.userPanSpeed = 0.01;
	controls.userZoomSpeed = 0.01;
	controls.userRotateSpeed = 0.01;
	controls.minPolarAngle = -Math.PI / 2;
	controls.maxPolarAngle = Math.PI / 2;
	controls.minDistance = 0.01;
	controls.maxDistance = 30;

	clock = new THREE.Clock();
	var delta = clock.getDelta();

	sparkSystem = new SparkSystem();
	
}

function animate() {
	requestAnimationFrame(animate);
	renderer.render(scene, camera);
	if (Globals.animate) {
		controls.update();
		postProcess();
	}
}

function rotate(object, axis, radians) {
	var rotObjectMatrix = new THREE.Matrix4();
	rotObjectMatrix.makeRotationAxis(axis.normalize(), radians);
	object.applyMatrix(rotObjectMatrix);
}
function translate(object, x, y, z) {
	var transObjectMatrix = new THREE.Matrix4();
	transObjectMatrix.makeTranslation(x, y, z);
	object.applyMatrix(transObjectMatrix);
}

function translatePosition(object, x, y, z) {
	/*
	var xTrans = object.position.x + x;
	var yTrans = object.position.y + y;
	var zTrans = object.position.z + z;
	object.position.set(xTrans,yTrans,zTrans);
	 */
	object.position.set(object.position.x + x, object.position.y + y, object.position.z + z);
}

function postProcess() {

	var delta = clock.getDelta();
	var asset = scene.getObjectByName("sawblade");
	var light = scene.getObjectByName("sparkLight");
	//light.intensity = getRandomInt(20,80);
	translate(asset, 0, -1.5, 0);
	rotate(asset, new THREE.Vector3(0, 0, 1), -9 * delta); //rotate sawblade
	translate(asset, 0, 1.5, 0);

	var sparkCreationRate = Utils.getRandomValue(sparkSystem.sparkCreationRate,sparkSystem.sparkCreationRateRandomness);
	for(var i=0;i<sparkCreationRate;i++)
		sparkSystem.createNewSpark();
	sparkSystem.updateSparks();
}

function OBJMesh(objpath, texpath, objName) {
	var texture = new THREE.TextureLoader(loadingManager).load(texpath, onLoad, onProgress, onError);
	var loader = new THREE.OBJLoader(loadingManager).load(objpath,
			function (object) {
			object.traverse(
				function (child) {
				if (child instanceof THREE.Mesh) {
					child.material.map = texture;
					child.material.needsUpdate = true;
				}

			});

			object.name = objName;
			//if(objName=="sawblade")
			//  translate(object, 0,1.5,0); //move it up to slab

			scene.add(object);
			afterMeshLoad(object);
			onLoad(object);
		},
			onProgress, onError);
}

function afterMeshLoad(mesh) {
	var position;
	switch(mesh.name) {
		case "cube" :
		  position = Globals.cubePosition;
		  sparkSystem.addCollisionObject(mesh);
		  break;
		case "sphere" :
		  position = Globals.spherePosition;
		  sparkSystem.addCollisionObject(mesh);
		  break
		case "bunny" :
		  position = Globals.bunnyPosition;
		  sparkSystem.addCollisionObject(mesh);
		  break
		case "cone" :
		  position = Globals.conePosition;
		  sparkSystem.addCollisionObject(mesh);
		  break
		case "ground" :
		  sparkSystem.addCollisionObject(mesh);
		  return;
		default :
			return;
	}
	var folder = datGui.addFolder(mesh.name);
	folder.add(position, "x").name("Position X").max(20).min(-20).onChange(function(value) {
	// Fires on every change, drag, keypress, etc.
	mesh.position.x = value;
	});;
	folder.add(position, "y").name("Position Y").max(20).min(-20).onChange(function(value) {
	// Fires on every change, drag, keypress, etc.
	mesh.position.y = value;
	});
	folder.add(position, "z").name("Position Z").max(20).min(-20).onChange(function(value) {
	// Fires on every change, drag, keypress, etc.
	mesh.position.z = value;
	});
	folder.add(mesh,"visible");
	
	mesh.position.set(position.x,position.y,position.z);
}

function onLoad(object) {
	putText(0, "", 0, 0);
	i_share++;
	if (i_share >= n_share)
		i_share = 0;
}

function onProgress(xhr) {
	if (xhr.lengthComputable) {
		var percentComplete = 100 * ((xhr.loaded / xhr.total) + i_share) / n_share;
		putText(0, Math.round(percentComplete, 2) + '%', 10, 10);
	}
}

function onError(xhr) {
	putText(0, "Error", 10, 10);
}

function putText(divid, textStr, x, y) {
	var text = document.getElementById("avatar_ftxt" + divid);
	text.innerHTML = textStr;
	text.style.left = x + 'px';
	text.style.top = y + 'px';
}

function putTextExt(dividstr, textStr) //does not need init
{
	var text = document.getElementById(dividstr);
	text.innerHTML = textStr;
}

window.onload = function () {
	init();
	drawControls();
	animate();
};
window.onclick = function () {
	if(Globals.createSparkOnMouseClick)
		sparkSystem.createNewSpark();
};
var datGui = new dat.GUI();
function drawControls() {
	var physicsFolder = datGui.addFolder("Physics");
	//physicsFolder.add(Globals, "physicsFactor").max(1).min(0).step(0.01);
	//.onFinishChange(function () {
	//	Globals.physicsFactor *= 0.01;
	//});
	physicsFolder.add(Globals, "airDrag").name("Air Density").max(10).min(0).step(0.2);

	var velocityFolder = datGui.addFolder("Velocity");
	//datGui.add(SparkSystem.velocity, "x").name("Velocity X").max(100).min(0).step(1);
	//datGui.add(SparkSystem.velocity, "y").name("Velocity Y").max(100).min(0).step(1);
	//datGui.add(SparkSystem.velocity, "z").name("Velocity Z").max(100).min(0).step(1);
	velocityFolder.add(sparkSystem.velocity, "x").name("X").max(20).min(-20).step(1);
	velocityFolder.add(sparkSystem.velocity, "y").name("Y").max(20).min(-20).step(1);
	velocityFolder.add(sparkSystem.velocity, "z").name("Z").max(20).min(-20).step(1);

	//datGui.add(SparkSystem.velocityRandomness, "x").name("VelocityRand X").max(100).min(0).step(1);
	//datGui.add(SparkSystem.velocityRandomness, "y").name("VelocityRand Y").max(100).min(0).step(1);
	//datGui.add(SparkSystem.velocityRandomness, "z").name("VelocityRand Z").max(100).min(0).step(1);
	var velocityRandomFolder = datGui.addFolder("Random Velocity");
	velocityRandomFolder.add(sparkSystem.velocityRandomness, "x").name("VelocityRand X").max(100).min(0).step(1);
	velocityRandomFolder.add(sparkSystem.velocityRandomness, "y").name("VelocityRand Y").max(100).min(0).step(1);
	velocityRandomFolder.add(sparkSystem.velocityRandomness, "z").name("VelocityRand Z").max(100).min(0).step(1);

	var positionFolder = datGui.addFolder("Spark Orign Position");
	positionFolder.add(sparkSystem.position, "x").name("Position X").max(10).min(-10).step(0.1);
	positionFolder.add(sparkSystem.position, "y").name("Position Y").max(10).min(-10).step(0.1);
	positionFolder.add(sparkSystem.position, "z").name("Position Z").max(10).min(-10).step(0.1);

	var positionRandomFolder = datGui.addFolder("Random Position");
	positionRandomFolder.add(sparkSystem.positionRandomness, "x").name("PositionRand X").max(100).min(0).step(1);
	positionRandomFolder.add(sparkSystem.positionRandomness, "y").name("PositionRand Y").max(100).min(0).step(1);
	positionRandomFolder.add(sparkSystem.positionRandomness, "z").name("PositionRand Z").max(100).min(0).step(1);

	var sparkFolder = datGui.addFolder("Spark");
	sparkFolder.add(sparkSystem, "sparkCreationRate").name("Sparks/Frame").max(50).min(0).step(1);
	sparkFolder.add(sparkSystem, "sparkCreationRateRandomness").name("Spark/Frame randomness").max(100).min(0).step(1);
	sparkFolder.add(sparkSystem, "sparkSize").name("Sparks/Frame").max(1).min(0.001).step(0.01);
	sparkFolder.add(sparkSystem, "sparkSizeRandomness").name("Spark/Frame randomness").max(100).min(0).step(1);
	sparkFolder.add(sparkSystem, "sparkLifespan").name("Sparks LifeSpan").max(200).min(1).step(1);
	sparkFolder.add(sparkSystem, "sparkLifespanRandomness").name("Spark LifeSpan randomness").max(100).min(0).step(1);
	sparkFolder.add(sparkSystem, "splitOnCollision").name("Split On Collision");

	datGui.add(Globals, "animate").name("Animate Scene");
	datGui.add(Globals, "createSparkOnMouseClick").name("Create on Click");
	
	

};
